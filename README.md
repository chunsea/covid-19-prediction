# fyp-final

# Introduction
This repository is a web application demo for Covid-19 based x-ray images classification

## 1. Install all requirements
```
cd covid19fyproj
pip install -r requirements.txt
```

## 2. set up SQL database 
```

python manage.py makemigrations
python manage.py migrate
```

## 3. Run the Server 

```
python manage.py runserver

```

