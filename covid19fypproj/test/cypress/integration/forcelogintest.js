//Test login for it_expert wrong password
describe('Check for wrong password', () => {
  it('successfully loads', () => {
    cy.visit('http://127.0.0.1:8000/login')

    cy.get('div.username')
      .type('expert_user')

    cy.get('div.password')
    .type('password')

    cy.contains('Sign in')
      .click()

    cy.url()
      .should('include', 'http://127.0.0.1:8000/login')

  })
})

//test for manual routes 
describe('see if unauthenticated user can edit add or delete photos' ,()=>{


  it('loads',()=>{

    cy.visit('http://127.0.0.1:8000/medical/new')

    cy.url()
      .should('include', 'http://127.0.0.1:8000/login')
  })
})