//Test Successful Display of Homepage
describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('127.0.0.1:8000')

  })
})


//Test login for medical_expert
describe('The Medical Expert Page', () => {
  it('successfully loads', () => {
    cy.visit('localhost:8000/login')

    cy.get('div.username')
      .type('yg1')

    cy.get('div.password')
    .type('yg1')

    cy.contains('Sign in')
      .click()

    cy.url()
      .should('include', 'localhost:8000/medical')

  })
})



//Test login for medical_expert
describe('Force Medical Expert users to login', () => {
  it('stays at login page if user is not logged in', () => {

    cy.visit('localhost:8000/medical')

  })
})



// describe('Attempt to redirect to IT Expert page but user role is Medical Expert', () => {
//   it('successfully loads', () => {
//     cy.visit('localhost:8000/login')

//     cy.get('div.username')
//       .type('yg1')

//     cy.get('div.password')
//     .type('yg1')

//     cy.contains('Sign in')
//       .click()

//     cy.url()
//       .should('include', 'localhost:8000/medical')

//   })
//   //cannot work chun hwee dai lou settle pls
//   it('should not redirect to IT Expert Page', () => {
//       cy.visit('localhost:8000/it_expert')
//   })
// })




// //Test login for it_expert
// describe('The IT Expert Page', () => {
//   it('successfully loads', () => {
//     cy.visit('localhost:8000/login')

//     cy.get('div.username')
//       .type('it_expert1')

//     cy.get('div.password')
//     .type('ITEPassword!')

//     cy.contains('Sign in')
//       .click()

//     cy.url()
//       .should('include', 'localhost:8000/it_expert')

//   })
// })