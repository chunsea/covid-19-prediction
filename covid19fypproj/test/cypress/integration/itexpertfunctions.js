//Test Successful Display of Homepage
describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('http://127.0.0.1:8000/')

  })
})


//Test login for it_expert
describe('The IT Expert Page', () => {
  it('successfully loads', () => {
    cy.visit('http://127.0.0.1:8000/login')

    cy.get('div.username')
      .type('expert_user')

    cy.get('div.password')
    .type('icesharkboi1')

    cy.contains('Sign in')
      .click()

    cy.url()
      .should('include', 'http://127.0.0.1:8000/it_expert')

  })
})












