from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator
from decimal import *
from django.utils.translation import gettext as _
from datetime import datetime   
from django.utils import timezone
import os
from decimal import Decimal
from django.core.exceptions import ValidationError
from sorl.thumbnail import ImageField, get_thumbnail
# Create your models here.
from uuid import uuid4

class User(AbstractUser):
    is_medical = models.BooleanField('Is_medical', default=False)
    is_expert = models.BooleanField('Is_expert', default=False)

    @property
    def is_Medical(self):
        return self.is_medical
    

    @property
    def is_Expert(self):
        return self.is_expert

def validate_file_extension(value):
   
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.png', '.jpeg', '.jpg']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Must be image with .png, .jpeg, .jpg extension')


def path_and_rename(path):
    def wrapper(instance, filename):
        
        day = timezone.now().day
        month = datetime.now().strftime("%B") # returns the name of the month
        year = timezone.now().year
        filename = f'{day}_{month}_{year}_{uuid4()}'


        # ext = filename.split('.')[-1]
        # # get filename
        # if instance.pk:
        #     filename = '{}.{}'.format(instance.pk, ext)
        # else:
        #     # set filename as random string
        #     filename = '{}.{}'.format(uuid4().hex, ext)
        # # return the whole path to the file
        return os.path.join(path, filename)
    return wrapper

from django.utils.deconstruct import deconstructible

@deconstructible
class PathRename:

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        day = timezone.now().day
        month = datetime.now().strftime("%B") # returns the name of the month
        year = timezone.now().year
        filename = f'{day}_{month}_{year}_{uuid4()}'
        # return the whole path to the file
        return os.path.join(self.path, filename)

path_and_rename = PathRename("/static/images/")



class expertdata(models.Model):

    GENDER = (
        ('male' , 'MALE'),
        ('female', 'FEMALE'),
        )

    CONTINENT = (
        ('ASIA PACIFIC' , 'Asia Pacific'),
        ('EUROPE', 'Europe'),
        ('AMERICA', 'America'),
        ('MIDDLE_EAST / AFRICA', 'Middle_East/Africa'),
        ('Others', 'Others'),
        )

    AGE_GROUPS = (
        ('1-20' , '1-20'),
        ('21-40', '21-40'),
        ('41-60', '41-60'),
        ('61-80', '61-80'),
        ('81-100','81-100'),       
        )

    MONTH = (

        )

    # RACES = (
    #     ('Chinese', 'Chinese'),
    #     ('Malay', 'Malay'),
    #     ('Indian', 'Indian'),
    #     ('White', 'White'),
    #     ('Black/African American','Black/African American'),
    #     ('Others','Others')

    #     )

    class Meta:
        ordering = ('-pk',)

    age = models.CharField(max_length = 10 , choices = AGE_GROUPS, default = '1-20',verbose_name ="Age Group") # must be positive 
    gender = models.CharField(max_length=30 ,choices= GENDER,default='male')
    continent = models.CharField(max_length=50,choices= CONTINENT , default ='Asia Pacific' )
    # race = models.CharField(max_length=50, choices = RACES,default ='Chinese')
    prediction = models.DecimalField(max_digits=4, decimal_places=2 ,validators =[MinValueValidator(Decimal('0.01'))])
    image = models.ImageField(null=True , upload_to='static/images/',validators=[validate_file_extension])
    # file = models.FileField(upload_to='static/images/', blank=True, null=True)
    Date = models.DateField(_("Date"), default=timezone.now)
    day = models.CharField(max_length=100, null=True, blank=False)
    month = models.CharField(max_length=100, null=True, blank=False)
    year = models.CharField(max_length=100, null=True, blank=False)


    def __str__(self):
        return str(self.id)


    def save(self , *args , **kwargs):
        
        if self.Date:
            self.day = timezone.now().day
            self.month = datetime.now().strftime("%B") # returns the name of the month
            self.year = timezone.now().year
        print(self.day , self.month ,self.year)

        # if self.image:
        #     self.image = get_thumbnail(self.image , '224x224', quality=99,format='JPEG').url

        super(expertdata , self).save(*args , **kwargs)





class Result(models.Model):

    userid = models.OneToOneField(expertdata, on_delete=models.CASCADE,primary_key =True)
    inceptionv3_result = models.DecimalField(max_digits=4, decimal_places=2 ,validators =[MinValueValidator(Decimal('0.0000000001'))])
    vgg16_result = models.DecimalField(max_digits=4, decimal_places=2 ,validators =[MinValueValidator(Decimal('0.00000000001'))])
    vgg19_result =models.DecimalField(max_digits=4, decimal_places=2 ,validators =[MinValueValidator(Decimal('0.00000000001'))])
    accuracy = models.DecimalField(max_digits=4, decimal_places=2 ,null=True)
    prediction = models.CharField(max_length=20, null=True)
    
    def compareTo(self):

        return max([self.inceptionv3_result, self.vgg16_result, self.vgg19_result])

    def positive_or_negative(self):

        count = 0

        if self.inceptionv3_result > 0.4:
            count+=1 
        if self.vgg16_result > 0.4:
            count+=1
        if self.vgg19_result > 0.4:
            count+=1


        return 'Positive' if count >= 2 else 'Negative'


        

    def save(self , *args , **kwargs):

        if self.inceptionv3_result and self.vgg16_result and self.vgg19_result:
            self.accuracy = self.compareTo()
            self.prediction = self.positive_or_negative()

        super(Result, self).save(*args , **kwargs)
    
    






