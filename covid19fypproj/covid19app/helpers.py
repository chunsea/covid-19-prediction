import cv2
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


def preprocess_image_from_infile_memory(img , shape=224):
    img = cv2.imdecode(np.frombuffer(img.read() , np.uint8), cv2.IMREAD_UNCHANGED)
    img = cv2.resize(img , (224,224)).astype('float32')
    img = img /255.
    
    img = tf.expand_dims(img , axis=0)
    print(img)
    img = np.reshape(img ,[1,shape,shape,3])


    return img


def load_and_prep_image(filename, img_shape=224):
  """
  Reads an image from filename, turns it into a tensor
  and reshapes it to (img_shape, img_shape, colour_channel).
  """
  # Decode the read file into a tensor & ensure 3 colour channels
  img = tf.io.read_file(filename)
 
  # (our model is trained on images with 3 colour channels and sometimes images have 4 colour channels)
  img = tf.image.decode_image(img, channels=3)

  # Resize the image (to the same size our model was trained on)
  img = tf.image.resize(img, size = [img_shape, img_shape])

  # Rescale the image (get all values between 0 and 1)
  img = img/255.
  return img