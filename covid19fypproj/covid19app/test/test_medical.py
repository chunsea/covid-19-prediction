from django.test import TestCase
from covid19app.models import User ,expertdata
from django.test import Client
from covid19app.forms import LoginForm
from django.contrib.messages import get_messages


c = Client()

class TestUser(TestCase):

	def setUp(self):
		self.user1 = User.objects.create(username = 'yg', is_medical = True)
		self.user1.set_password('password')
		self.user1.save()

		self.credentials = {
            'username': 'yg',
            'password': 'password'}


	def test_if_medical_is_medical(self):
		#print('test step 1.1 - Medical user should be able to see the login button on the sign in page')

		self.assertEqual(self.user1.is_Medical, True)
		self.assertEqual(self.user1.is_Expert, False)

	def test_if_users_can_login_to_medical_page(self):

		self.client.login(username= self.user1.username , password = self.user1.password)
		response = self.client.get('/medical', follow=True)
		self.assertEqual(response.status_code, 200)

	def test_wrong_username_password(self):

		client = self.client.login(username = 'wrongid', password= 'wrong_password')

		self.assertFalse(client , 404)

	def test_error_message(self):

		response = self.client.post('/login', {'yg1':'admin'}) # wrong password
		messages = list(response.context['messages'])
		self.assertEqual(len(messages), 1)


	def test_check_if_username_is_correct_then_redirect_to_next_page(self):

		response = self.client.post('/login', {'yg1': 'password'})
		self.assertEqual(response.status_code , 200) # redirects

	
	def test_to_see_if_non_logged_in_users_can_access_to_home_page(self):


		response  = c.get('/')
		self.assertEqual(response.status_code , 200)

	def test_to_see_if_non_logged_in_users_cannot_access_expert_page(self):


		response2 = c.get('/it_expert')

		self.assertEqual(response2.status_code , 302)
	

	def test_to_see_if_non_logged_in_users_cannot_access_medical_page(self):

		response = c.get('/medical')

		self.assertEqual(response.status_code , 302)



# Create your tests here.
