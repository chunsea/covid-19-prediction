### what is this ?

### this is a django unit testing framework

- to run , compile the following commands 

'''python
	python manage.py test <appname>.<file_directory>.<filename>

	-for example, 
	python manage.py test covid19app.test.test_user.py
'''