from celery import shared_task
import time
from .helpers import preprocess_image_from_infile_memory
from .models import expertdata

@shared_task(bind=True)
def go_to_sleep(self, duration):
	for i in range(5):
		time.sleep(duration)
	return 'done'


@shared_task
def add(x, y):
    return x + y


#upload images then predict
@shared_task(bind=True)
def upload_image_then_predict(self, img,img_shape=224):

	return preprocess_image_from_infile_memory(img, img_shape)



@shared_task(bind=True)
def get_all_data(self):


	data = expertdata.objects.all()

	return {'data': data}




