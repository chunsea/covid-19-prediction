# Generated by Django 4.0.4 on 2022-05-19 08:49

import covid19app.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('covid19app', '0013_result'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='expertdata',
            name='race',
        ),
        migrations.AlterField(
            model_name='expertdata',
            name='continent',
            field=models.CharField(choices=[('ASIA PACIFIC', 'Asia Pacific'), ('EUROPE', 'Europe'), ('AMERICA', 'America'), ('MIDDLE_EAST / AFRICA', 'Middle_East/Africa'), ('Others', 'Others')], default='Asia Pacific', max_length=50),
        ),
        migrations.AlterField(
            model_name='expertdata',
            name='image',
            field=models.ImageField(null=True, upload_to=covid19app.models.PathRename('/static/images/'), validators=[covid19app.models.validate_file_extension]),
        ),
    ]
