# Generated by Django 4.0.4 on 2022-05-21 06:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('covid19app', '0020_alter_result_accuracy'),
    ]

    operations = [
        migrations.AlterField(
            model_name='result',
            name='prediction',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
