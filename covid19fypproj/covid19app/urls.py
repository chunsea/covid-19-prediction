"""covid19fypproj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views
from .data_tables import ExpertListJson
from django.conf.urls.static import static
from django.conf import settings

app_name = 'covid19app'


urlpatterns = [
    path('', views.home, name="home"),
    path('logout', views.logout_view, name = 'logout'),
    path('login', views.login_page, name="login"),
    path('medical', views.medical, name="medical"),
    path('medical/<int:pk>', views.medical_find ,name='medical_find'),
    path('medical/new', views.addRecord, name="addRecord"),
    path('medical/<int:pk>/edit', views.edit_record, name = 'editRecord'),
    path('medical/<int:pk>/view', views.view_record , name = 'viewRecord'),
    path('medical/<int:pk>/delete', views.delete_record, name = 'deleteRecord'),
    path('medical/<int:pk>/delete_record', views.view_deleted_record , name = 'view_deleted_record'),
    path('datatable/data' ,ExpertListJson.as_view() , name = 'expert_list_json'),
    path('it_expert', views.IT_expert, name="it_expert"),
    path('help', views.help_page, name='help_page'),
    path('new', views.test_celery_route , name = 'name_page'),
    path('new/<uuid:task_id>', views.test_view_celery_result, name = 'test_view_celery_result'),
    


    


]




urlpatterns  += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
