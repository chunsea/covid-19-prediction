from django_datatables_view.base_datatable_view import BaseDatatableView
from django.utils.html import escape
from .models import expertdata

class ExpertListJson(BaseDatatableView):

	model = expertdata
	# define the columns that will be returned
	columns = ['id','age','gender', 'country', 'prediction']

	max_display_length = 500

	order_columns = ['age', 'gender','country', 'prediction']

