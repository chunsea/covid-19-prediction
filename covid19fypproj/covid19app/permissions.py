from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import redirect , render
# from django.contrib.auth.decorators import user_passes_test
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.4 fallback.
from django.contrib import messages
from django.http import Http404
from django.http import HttpResponseNotFound


default_message = "Please enter username and password"


def logout_excluded(redirect_to):
    """ This decorator kicks authenticated users out of a view """ 
    def _method_wrapper(view_method):
        def _arguments_wrapper(request, *args, **kwargs):
            if request.user.is_anonymous:
                return redirect(redirect_to) 
            return view_method(request, *args, **kwargs)
        return _arguments_wrapper
    return _method_wrapper





def login_excluded(redirect_to):
    """ This decorator kicks authenticated users out of a view """ 
    def _method_wrapper(view_method):
        def _arguments_wrapper(request, *args, **kwargs):
            if request.user.is_authenticated:
                return redirect(redirect_to) 
            return view_method(request, *args, **kwargs)
        return _arguments_wrapper
    return _method_wrapper


def admin_only(test_func, message=default_message):
    """
    Decorator for views that checks that the user passes the given test,
    setting a message in case of no success. The test should be a callable
    that takes the user object and returns True if the user passes.
    """
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if not test_func(request.user):
                messages.error(request, message)
                raise Http404("Poll does not exist")
            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator






def user_passes_test(test_func, message=default_message, login_url = 'covid19app:login'):
    """
    Decorator for views that checks that the user passes the given test,
    setting a message in case of no success. The test should be a callable
    that takes the user object and returns True if the user passes.
    """
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if not test_func(request.user):
                messages.error(request, message)
                return redirect(login_url)
            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator

def expert_required(function=None, message=default_message):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated and u.is_Expert , #fixed by removing ()
        message=message,
       
    )
    if function:
        return actual_decorator(function)
    return actual_decorator 



def medical_required(function=None, message=default_message):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated and u.is_Medical , #fixed by removing ()
        message=message,
    )
    if function:
        return actual_decorator(function)
    return actual_decorator        




# def superuser_required(view_func = None,  redirect_field_name = REDIRECT_FIELD_NAME,\
# 	login_url = 'login'
# 	):
	
# 	'''
# 	Decorator for views that checks that the user is logged in and is a superuser , redirecting to the
# 	login if necessary
# 	'''

# 	actual_decorator = user_passes_test(
# 		lambda u : u.is_active and u.is_superuser,
# 		login_url = login_url,
# 		redirect_field_name = redirect_field_name
# 		)

# 	if view_func:
# 		return actual_decorator(view_func)

# 	return actual_decorator


# def it_expert_requried(view_func = None,  redirect_field_name = REDIRECT_FIELD_NAME,\
# 	login_url = 'login'
# 	):
	
# 	'''
# 	Decorator for views that checks that the user is logged in and is a superuser , redirecting to the
# 	login if necessary
# 	'''

# 	actual_decorator = user_passes_test(
# 		lambda u : u.is_active and u.is_superuser,
# 		login_url = login_url,
# 		redirect_field_name = redirect_field_name
# 		)

# 	if view_func:
# 		return actual_decorator(view_func)

# 	return actual_decorator