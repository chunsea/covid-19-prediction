from django.apps import AppConfig
from django.conf import settings
import os
import tensorflow as tf


class Covid19AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'covid19app'
    print('starting web app')
    path = settings.MODELS

    vgg16 = tf.keras.models.load_model(f'{path}/vgg16_model.h5')

    # vgg16.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
    #           loss=tf.keras.losses.BinaryCrossentropy())

    inceptionv3 = tf.keras.models.load_model(f'{path}/inceptionv3_model.h5')

    # inceptionv3.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
    #           loss=tf.keras.losses.BinaryCrossentropy())

    vgg19 = tf.keras.models.load_model(f'{path}/vgg19_model.h5')

    # resnet50.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
    #           loss=tf.keras.losses.BinaryCrossentropy())


