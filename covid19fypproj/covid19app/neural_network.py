import tensorflow as tf
from .models import expertdata , Result
import cv2
import numpy as np

img_width = 224
img_height = 224


def process_data(img_path):
    img = cv2.imread(img_path)
    img = cv2.resize(img, (img_width, img_height))
    img = img/255.0
    
    return img



def compose_total_dataset():
	data = []
	labels = []

	image_val = expertdata.objects.all()



	for i in image_val:
	    data.append(process_data(f'{i.image}'))
	    if i.prediction > 40:

	    	labels.append(1)
	    else:
	    	labels.append(0)
	    
	return np.array(data), np.array(labels)


# def compose_train_test_split(percentage):

# 	training = 

# 	testing  = 

# 	return training , testing







class Vgg16:

	def __init__(self):
		pass


class Resnet50:

	def __init__(self):
		pass


class inceptionV3:

	def __init__(self):
		pass

class hyperParameterTuning:


	def __init__(self):
		self.models = ['vgg16', 'resnet50', 'inceptionv3']
		self.vgg16 = Vgg16()
		self.resnet50 = Resnet50()
		self.inceptionV3 = inceptionV3()

	def check_if_model_exist(self, name):

		if name.lower() in self.models:
			return name

	def run(self):
		pass
