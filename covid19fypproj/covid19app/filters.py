import django_filters
from .models import expertdata

class ImageFilter(django_filters.FilterSet):

	name = django_filters.CharFilter(lookup_expr = 'iexact')

	class Meta:
		model = expertdata
		fields = ['age', 'continent', 'gender']
