from django.contrib import admin
from .models import expertdata , User
from django.conf import settings
from django.contrib.auth.admin import UserAdmin

from django.contrib.auth.forms import UserChangeForm

class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class MyUserAdmin(UserAdmin):
    form = MyUserChangeForm

    fieldsets = (
        (None, {
            'fields': ('username', 'password',  'is_medical' ,'is_expert', 'is_superuser','is_staff')
        }),

      
    )



# Register your models here.
admin.site.register(User, MyUserAdmin)

admin.site.register(expertdata)