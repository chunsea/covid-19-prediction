from django import forms
from django.contrib.auth.models import User
from .models import User, expertdata
from django.forms import ClearableFileInput
import os
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.core.validators import MinValueValidator
from decimal import Decimal

# Create your forms here.

class LoginForm(forms.Form):
	username = forms.CharField(
		widget = forms.TextInput(
			attrs = {
				"class": "form-control"
			}
		)
	)

	password = forms.CharField(
		widget = forms.PasswordInput(
			attrs = {
				"class": "form-control"
			}
		)
	)
	class Meta:
		model = User
		fields = ("username", "password", 'Is_medical', 'Is_expert')


def validate_file_extension(value):
   
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.png', '.jpeg', '.jpg']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Must be image with .png, .jpeg, .jpg extension')


class ImageForm(forms.ModelForm):

	files = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),validators =[validate_file_extension])

	class Meta:
		model = expertdata
		fields = ['age' , 'continent' , 'gender' , 'files']






class PublicForm(forms.ModelForm):

	# file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
	# img = forms.FileField(validators =[validate_file_extension])


	def clean_image_data(self):

		return self.cleaned_data.get('image')

	class Meta:
		model = expertdata
		#Age group
		fields = ['age', 'continent', 'gender' , 'image']
		# widgets = {
  #           'file': ClearableFileInput(attrs={'multiple': True})
  #       }

class ExpertForm(forms.Form):

	class Meta:
		fields = ['dataset_directory', 'Training_data_ratio', 'batch_size', 'Models_to_use',\
		'Learning_Rate', 'Activation' , 'Regularization' , 'Regularization_Rate'
		]
	


class Covid19Form(forms.Form):

	#do not allow them to let them choose

	LOW = 16
	NORMAL = 32
	TEN_PERCENT = 10
	TWENTY_PERCENT = 20
	THIRTY_PERCENT = 30
	BATCH_SIZE_CHOICES = (
    (LOW, '16'),
    (NORMAL, '32'),
    )

    

	# only 10% 20% or 30%
	TRAINING_RATIO_CHOICES = (
		(TEN_PERCENT, '10'),
		(TWENTY_PERCENT , '20'),
		(THIRTY_PERCENT, '30'),
		)
    #vgg16 , resnet50 , inception v3

	VGG16 = 'vgg16'
	VGG19 = 'vgg19'
	INCEPTIONV3 = 'inceptionv3'


	MODELS_TO_USE_CHOICES =(
		(VGG16 , 'VGG16'),
		(VGG19 , 'VGG19'),
		(INCEPTIONV3, 'InceptionV3'),
		)

    
	SOFTMAX = 'softmax'
	SIGMOID = 'sigmoid'
	RELU = 'relu'


	ACTIVATION_METHOD_CHOICES = (
		(SOFTMAX, 'softmax'),
		(SIGMOID, 'sigmoid'),
		(RELU, 'relu'),
		)



	# directory = forms.CharField(max_length=100) # maybe remove this 
	training_ratio = forms.TypedChoiceField(choices = TRAINING_RATIO_CHOICES, label='Training Ratio',initial=10,\
		help_text = 'Refers to the percentage to be allocated for test'
		)
	batch_size = forms.TypedChoiceField(choices=BATCH_SIZE_CHOICES,label='Batch Size',initial=16,\
		help_text = 'Refers to the number of training examples utilized in one iteration. Use either 16 or 32'
		)
	models_to_use = forms.TypedChoiceField(choices = MODELS_TO_USE_CHOICES , label='Models to Use', initial='vgg16',\
		help_text =mark_safe('One of the three models to use for deep learning. <a href="https://keras.io/api/applications/vgg/">Read more here</a>.')
		)
	learning_rate = forms.DecimalField(decimal_places=5 ,validators =[MinValueValidator(Decimal('0.00001'))], max_digits = 6,label='Learning Rate',\
		help_text = mark_safe('Set to allow models to learn x-ray images at a lower rate  <a href="/help">Read more here</a>.'),
		)
	activation_method = forms.TypedChoiceField(choices = ACTIVATION_METHOD_CHOICES, label = 'Activation Method' , \
		help_text='Refers to the activation method to train the models',required=False)
	
	



