from django.shortcuts import render , redirect, get_object_or_404
from django.urls import reverse , reverse_lazy
from django.http import HttpResponse
from django.contrib.auth  import  authenticate , login , logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm #add this
from .forms import LoginForm , ImageForm , PublicForm , Covid19Form, ExpertForm
from .models import expertdata ,Result
from .filters import ImageFilter
from .permissions import medical_required , expert_required , login_excluded , logout_excluded , admin_only
from django.contrib.auth.decorators import user_passes_test 
import logging
from django.http import HttpResponse
import matplotlib.pyplot as plt
import tensorflow as tf
from PIL import Image
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
import cv2
from .neural_network import hyperParameterTuning

from celery.result import AsyncResult
from keras.preprocessing import image
from .apps import Covid19AppConfig
from .task import go_to_sleep , add , get_all_data



@login_required(login_url='covid19app:login')
def test_celery_route(request):

    # sleep = go_to_sleep.delay(10)
    sleep = get_all_data.delay()
    

    return render(request, 'covid19app/new.html', {'task_id': sleep})


@login_required(login_url='covid19app:login')
def test_view_celery_result(request , task_id):

    status = AsyncResult(task_id)
    result = None

    if status.state == 'SUCCESS':
        result =status.result




    

    return render(request, 'covid19app/result.html', {'task_id': task_id, \
        'status' :status , 'result': result})




# model = tf.keras.models.load_model('static/models/vgg16_model.h5',\
#   custom_objects={'KerasLayer': hub.KerasLayer})


class_names = ['NEGATIVE','POSITIVE']

# Create your views here.


def load_and_prep_image(filename, img_shape=224):
  """
  Reads an image from filename, turns it into a tensor
  and reshapes it to (img_shape, img_shape, colour_channel).
  """
  # Decode the read file into a tensor & ensure 3 colour channels
  img = tf.io.read_file(filename)
 
  # (our model is trained on images with 3 colour channels and sometimes images have 4 colour channels)
  img = tf.image.decode_image(img, channels=3)

  # Resize the image (to the same size our model was trained on)
  img = tf.image.resize(img, size = [img_shape, img_shape])

  # Rescale the image (get all values between 0 and 1)
  img = img/255.
  return img




def home(request): # public user page
    form = PublicForm()
    pred = None
    value = None
    if request.method == 'POST':
        form = PublicForm(request.POST , request.FILES)
       
        if form.is_valid():
            print('chun was here!')
           
            new_file = form.save(commit=False)
            obj = expertdata(age = new_file.age, continent = new_file.continent , \
                    gender = new_file.gender , image = new_file.image, prediction = 25)
            obj.save() #save the instance
            messages.success(request, f'successfully uploaded files!')

            data = expertdata.objects.first()
            print(data.image)


            

            img = image.load_img(f'{data.image}',target_size=(224,224))
            img = np.asarray(img)
            img = np.expand_dims(img, axis=0)
            print(img.shape)

            vgg16 , vgg19 , inceptionv3 = 0 , 0 ,0 

            output = Covid19AppConfig.vgg16.predict(img)
            if output[0][0] > output[0][1]:
                print("POSITIVE")
                print(output[0][0])
                print(output[0][1])
                pred = 'Positive' 
                vgg16 = float(round(output[0][1], 2))

            else:
                print('NEGATIVE')
                print(output[0][0])
                print(output[0][1])
                pred = 'Negative'
                vgg16 = float(round(output[0][0], 2))

            output1 = Covid19AppConfig.vgg19.predict(img)
            if output1[0][0] > output1[0][1]:
                print("POSITIVE")
                print(output1[0][0])
                print(output1[0][1])
                pred = 'Positive' 
                vgg19 = float(round(output1[0][1], 2))
                value = vgg19

            else:
                print('NEGATIVE')
                print(output1[0][0])
                print(output1[0][1])
                pred = 'Negative'
                vgg19 = float(round(output1[0][0], 2))
                value = vgg19


            output2 = Covid19AppConfig.inceptionv3.predict(img)
            if output2[0][0] > output2[0][1]:
                print("POSITIVE")
                print(output2[0][0])
                print(output2[0][1])
                # pred = 'Positive' 
                inceptionv3 = float(round(output2[0][1], 2))

            else:
                print('NEGATIVE')
                print(output2[0][0])
                print(output2[0][1])
                # pred = 'Negative'
                inceptionv3 = float(round(output2[0][0] , 2))


            result = Result.objects.create(userid=data, inceptionv3_result=inceptionv3,\
                    vgg16_result=vgg16,\
                    vgg19_result=vgg19)
           

            data.prediction = float(value) * 100

            data.save()

            # pred = class_names[int(tf.round(prediction[0][0]))]
            # print(prediction[0][0])
                # return redirect(redirect('covid19app:home', kwargs = {'form': form}))
        # might be a problem when i refresh
        # its either i can redirect to request another page or something else
            return render(request , 'covid19app/home.html', {'form': form ,"obj" : obj , "pred" : pred })
   
    return render(request, "covid19app/home.html", {'form': form , "pred": pred })



@login_excluded('covid19app:home')
def login_page(request):


    form = LoginForm(request.POST or None)
    msg = None
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_Medical:
                login(request, user)
                messages.success(request, 'successfully logged in ')
                return redirect("covid19app:medical")
            elif user is not None and user.is_Expert:
                login(request, user)
                messages.success(request, 'successfully logged in ')
                return redirect("covid19app:it_expert")
            else:
                messages.error(request, 'Username or password is wrong, please try again.')
                return redirect('covid19app:login')
        else:
            messages.error(request, 'error, please check again.')
    return render(request, 'covid19app/login.html', {'form':form, 'msg':msg})

 



@logout_excluded('covid19app:home')
def logout_view(request):
    logout(request)
    messages.info(request, 'successfully logged out')
    return redirect('covid19app:home')


@medical_required
def medical(request):

    form2 = Covid19Form(request.POST or None)

    
    return render(request, 'covid19app/medical.html', {'data':expertdata.objects.all().reverse() , 'form2': form2})

def medical_find(request , pk):

    data = expertdata.objects.get(pk = pk)
    print(data)
    context = {
        'data' : data
    }

    return render(request , 'covid19app/medical_view.html', context)


# @login_required(login_url='login')


@expert_required
def IT_expert(request):

   
    form2 = Covid19Form(request.POST or None)


    data = expertdata.objects.all().reverse()

    if request.method =='POST' and form2.is_valid():
        training_ratio = form2.cleaned_data.get('training_ratio')
        learning_rate = form2.cleaned_data.get('learning_rate')
        print(training_ratio , learning_rate)

    return render(request, 'covid19app/it_expert.html' , {'form2': form2 , 'data': data})


def preprocess_image_from_infile_memory(img , shape=224):
    img = cv2.imdecode(np.frombuffer(img.read() , np.uint8), cv2.IMREAD_UNCHANGED)
    img = cv2.resize(img , (224,224))
    img = np.asarray(img).astype(np.float32)
    img = img /255.
    
    print(img.shape)
    img = np.expand_dims(img , axis=0)
    img = np.reshape(img , [1,shape,shape,3])

    return img



@medical_required
def addRecord(request):
    '''
        here we add in more images 
        change / add in 1 more file to expertdata , filefield to accept mutiple images
        insert codes for machine learning
    '''
    form = ImageForm()
    
    #[{'name': }] 


    total = []
    if request.method == 'POST':
        form = ImageForm(request.POST , request.FILES)
        files = request.FILES.getlist('files')
        count = len(files)
        if form.is_valid():
            new_file = form.save(commit=False)
            print('chun was here!')
            count = 0



            for i in request.FILES.getlist('files'):
                # print(i.name)
                # img = preprocess_image_from_infile_memory(i.name)
                # prediction = Covid19AppConfig.vgg16.predict(img)

                # prediction = prediction[0][0] * 100
               
                obj = expertdata(age = new_file.age, continent = new_file.continent , \
                        gender = new_file.gender , image = i, prediction = 25)
                count+= 1
                obj.save() #save the instance


                 
            predict = expertdata.objects.all()[:count]
            for i in predict:
                d ={}
                prediction = load_and_prep_image(f'{i.image}', 224)
                prediction3 = Covid19AppConfig.vgg16.predict(tf.expand_dims(prediction,axis=0))

                prediction1 = Covid19AppConfig.inceptionv3.predict(tf.expand_dims(prediction,axis=0))
                prediction2 = Covid19AppConfig.vgg19.predict(tf.expand_dims(prediction,axis=0))


                vgg16 , inceptionv3 , resnet50 = 0 , 0 , 0


                pred1 ,pred2= prediction3[0][0] , prediction3[0][1]
                
                inception1 , inception2 = prediction1[0][0] , prediction1[0][1]

                resnet50_1 , resnet50_2 = prediction2[0][0] , prediction2[0][1]


                #inception
                if inception1 > inception2:
                    print('positive')
                    print(inception1)
                    print(inception2)
                    pred = class_names[int(tf.round(inception2))]
                    d['inceptionv3'] = pred
                    inceptionv3 = float(round(inception2,2))

                    
                else:
                    print('negative')
                    print(inception1)
                    print(inception2)
                    pred = class_names[int(tf.round(inception1))]
                    d['inceptionv3'] = pred
                    inceptionv3 = float(round(inception1, 2))

                #resnet50
                if resnet50_1 > resnet50_2:
                    print('positive')
                    print(resnet50_1)
                    print(resnet50_2)
                    pred = class_names[int(tf.round(resnet50_2))]
                    d['resnet50'] = pred
                    resnet50 = float(round(resnet50_2, 2))
                    

                else:
                    print('negative')
                    print(resnet50_1)
                    print(resnet50_2)
                    pred = class_names[int(tf.round(resnet50_1))]
                    d['resnet50'] = pred
                    resnet50 = float(round(resnet50_1, 2))


                #vgg16
            
                if pred1 > pred2:
                    print('positive')
                    print(pred1)
                    print(pred2)
                    pred = class_names[int(tf.round(pred2))]
                    d['vgg16'] = pred
                    vgg16 = float(round(pred2 , 2))

                else:
                    print('negative')
                    print(pred1)
                    print(pred2)
                    pred = class_names[int(tf.round(pred1))]
                    d['vgg16'] = pred
                    vgg16 = float(round(pred1 , 2))



                #save results in 
                p , result = Result.objects.get_or_create(userid=i, inceptionv3_result=inceptionv3,\
                    vgg16_result=vgg16,\
                    vgg19_result=resnet50)



                total.append(p)

            # set orm data to update database


            messages.success(request, f'successfully uploaded {count} files!')
        # might be a problem when i refresh
        # its either i can redirect to request another page or something else
       
        return render(request , 'covid19app/addRecord.html', {'form': form  , 'count' : count, \
            'total': total
            })
   
    return render(request, "covid19app/addRecord.html", {'form': form })

    



@login_required(login_url='covid19app:login')
def view_record(request, pk):

    obj = expertdata.objects.get(pk =pk)

    form = ImageForm(request.GET , instance =obj)

    if request.user.is_Medical:

        return render(request, 'covid19app/medical_view.html',{'form': form ,'data': obj})


    

@login_required(login_url='covid19app:login')
def edit_record(request , pk):

    obj = expertdata.objects.get(pk = pk)

    form = PublicForm(request.POST or None , instance =obj)

    if request.method == 'POST' and form.is_valid():
        form.save()

        messages.success(request, 'successfully edited record')


        
    return render(request, 'covid19app/medical_edit.html', {'form':form, "obj": obj})
       
            


@login_required(login_url='covid19app:login')
def delete_record(request, pk):


    obj = expertdata.objects.get(pk = pk)

    if obj:
        obj.delete()
        messages.success(request, 'record successfully deleted')

        if request.user.is_Medical:
            return redirect('covid19app:medical')
        else:
            return redirect('covid19app:it_expert')

    return render(request , 'covid19app/medical.html', {})


@login_required(login_url='covid19app:login')
def view_deleted_record(request , pk):

    obj = expertdata.objects.get(pk = pk)

    form = ImageForm(request.POST or None , instance=obj)

    if request.POST and form.is_valid():
        form.save()
        messages.success(request, 'successfully edited record')
        return redirect('covid19app:medical')

   
    return render(request , 'covid19app/partials/edit_record.html', {'form': form})

# def new_pages(request):

#     return render(request, 'covid19app/new.html', {})


def help_page(request):
    return render(request, 'help.html', {})   
       


#404 view 
def page_not_found_view(request, exception):
    return render(request, 'covid19app/404.html', status=404)



#500 view 
def handler500(request):

    
    return render(request, 'covid19app/500.html', status=500)




